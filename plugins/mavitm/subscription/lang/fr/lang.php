<?php return [
    'plugin' => [
        'name' => 'Subscription',
        'description' => '',
    ],
    'subtype' => [
        'name' => 'Name',
    ],
    'subscriber' => [
        'data' => 'Data',
        'subtype' => 'Subscriber type',
        'accepst' => 'Active',
        'content_id' => 'Content ID',
        'content_id_desc' => 'Other contetn row id',
        'content_type' => 'Content type',
        'content_type_desc' => 'Data model name or your group name for content_id',
    ],
    'menu' => [
        'subscriber' => 'Subscriber',
        'import' => 'Import',
        'export' => 'Export',
    ],
    'permission' => [
        'subscriber' => 'subscriber manage',
    ],
    'component' => [
        'csrf_error'            => 'Votre session est expiré! Veuillez rafraîchir la page.',
        'empty_error'           => 'Veuillez ne pas laisser d\'espace.',
        'in_success_text'       => 'Votre inscription est enregistrée',
        'out_success_text'      => 'Votre abonnement a été annulé',
        'save_error'            => 'Veuillez réessayer plus tard',
        'email_error'           => '%s n\'est pas une adresse e-mail valide',
        'validateEmail'         => 'Validate e-mail',
    ],
];
