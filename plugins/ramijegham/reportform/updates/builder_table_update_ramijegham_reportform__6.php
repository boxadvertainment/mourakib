<?php namespace RamiJegham\ReportForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRamijeghamReportform6 extends Migration
{
    public function up()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->dropColumn('file');
        });
    }
    
    public function down()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->string('file', 191)->nullable();
        });
    }
}
