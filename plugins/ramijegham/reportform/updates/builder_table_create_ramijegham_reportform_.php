<?php namespace RamiJegham\ReportForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRamijeghamReportform extends Migration
{
    public function up()
    {
        Schema::create('ramijegham_reportform_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigInteger('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('report_type');
            $table->string('file')->nullable();
            $table->string('age')->nullable();
            $table->text('description')->nullable();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ramijegham_reportform_');
    }
}
