<?php namespace RamiJegham\ReportForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRamijeghamReportform5 extends Migration
{
    public function up()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->renameColumn('grender', 'gender');
        });
    }
    
    public function down()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->renameColumn('gender', 'grender');
        });
    }
}
