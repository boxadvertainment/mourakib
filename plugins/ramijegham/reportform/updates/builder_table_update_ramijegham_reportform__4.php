<?php namespace RamiJegham\ReportForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRamijeghamReportform4 extends Migration
{
    public function up()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->string('grender')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->dropColumn('grender');
        });
    }
}
