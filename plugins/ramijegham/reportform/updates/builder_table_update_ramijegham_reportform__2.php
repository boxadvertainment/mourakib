<?php namespace RamiJegham\ReportForm\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRamijeghamReportform2 extends Migration
{
    public function up()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->bigIncrements('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('ramijegham_reportform_', function($table)
        {
            $table->bigInteger('id')->change();
        });
    }
}
