<?php namespace RamiJegham\ReportForm;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return[
            'RamiJegham\ReportForm\Components\ReportForm' => 'ReportForm'
        ];
    }

    public function registerFormWidgets()
    {
        return[
            'RamiJegham\ReportForm\FormWidgets\FilePreview' => [
                'label' => 'File Preview',
                'code' => 'filePreview'
            ]
        ];
    }

    public function registerSettings()
    {
    }
}
