<?php namespace RamiJegham\ReportForm\FormWidgets;

use Backend\Classes\FormWidgetBase;

use RamiJegham\reportForm\Models\Report;

class FilePreview extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'filePreview';

    public function widgetDetails()
    {
        return [
            'name' => 'File Preview',
            'description' => 'File Preview'
        ];
    }

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('widget');
    }

    public function prepareVars()
    {
        if ($this->model->file) {
            $this->vars['file'] = $this->model->file->getPath() ;
        } else {
            $this->vars['file'] =  null;
        }
    }
}
