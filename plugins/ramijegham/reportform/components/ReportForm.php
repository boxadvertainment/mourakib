<?php namespace RamiJegham\ReportForm\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use ValidationException;
use Flash;

use RamiJegham\reportForm\Models\Report;

class ReportForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Report Form',
            'description' => 'Report Abuse Form'
        ];
    }


    public function onSubmit()
    {

        $data = post();

        $rules = [
            'fullname' => 'required',
            'email' => 'required|email',
            'report_type' => 'required'
        ];


        $validation = Validator::make($data, $rules);

        if ( $validation->fails() ) {
            throw new ValidationException($validation);
        }

        $report = new Report();

        $report->fullname       = Input::get('fullname') ;
        $report->report_type    = Input::get('report_type') ;
        $report->email          = Input::get('email') ;
        $report->phone          = Input::get('phone') ;
        $report->age            = Input::get('age') ;
        $report->gender         = Input::get('gender') ;
        $report->description    = Input::get('description') ;
        $report->file           = Input::file('file') ;

        $report->save();

        Flash::success('Merci! Votre signalement a bien été enregistré');
    }


}
