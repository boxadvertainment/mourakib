<?php namespace RamiJegham\ReportForm\Models;

use Model;

/**
 * Model
 */
class Report extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ramijegham_reportform_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    public $attachOne = [
        'file' => 'System\Models\File'
    ];
}
