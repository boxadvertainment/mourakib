<?php namespace RamiJegham\ReportForm\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use RamiJegham\ReportForm\Models\Report;

class Reports extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $this->vars['totalReports'] = Report::all()->count();

        $this->vars['male'] = Report::where('gender', 'male')->get()->count();
        $this->vars['female'] = Report::where('gender', 'female')->get()->count();

        $this->vars['reportSex'] = Report::where('report_type', 'Exploitation sexuelle')->get()->count();
        $this->vars['reportFraud'] = Report::where('report_type', 'Fraude en lignes')->get()->count();
        $this->vars['reportSecurite'] = Report::where('report_type', 'Sécurité')->get()->count();

        $this->vars['age1'] = Report::where('age', '5 - 9ans')->get()->count();
        $this->vars['age2'] = Report::where('age', '10 - 18ans')->get()->count();
        $this->vars['age3'] = Report::where('age', '19 - 40ans')->get()->count();
        $this->vars['age4'] = Report::where('age', '41ans - plus')->get()->count();
        $this->vars['age5'] = Report::where('age', null)->get()->count();


        $this->asExtension('ListController')->index();
    }
}
