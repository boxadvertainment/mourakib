<?php namespace RamiJegham\Contact\Models;

use Model;

/**
 * Model
 */
class Contact extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'ramijegham_contact_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
