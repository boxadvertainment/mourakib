<?php namespace RamiJegham\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRamijeghamContact extends Migration
{
    public function up()
    {
        Schema::create('ramijegham_conatct_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('fullname');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->text('message');
            $table->string('subject');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ramijegham_contact_');
    }
}
