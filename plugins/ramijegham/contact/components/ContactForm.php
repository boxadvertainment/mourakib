<?php namespace RamiJegham\Contact\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use ValidationException;
use Flash;

use RamiJegham\Contact\Models\Contact;

class ContactForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Contact Form',
            'description' => 'Contact Abuse Form'
        ];
    }


    public function onSubmit()
    {

        $data = post();

        $rules = [
            'fullname' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ];


        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $contact = new Contact();

        $contact->fullname       = Input::get('fullname') ;
        $contact->email          = Input::get('email') ;
        $contact->phone          = Input::get('phone') ;
        $contact->subject        = Input::get('subject') ;
        $contact->message        = Input::get('message') ;

        $contact->save();

        Flash::success('Merci! Votre demande de contact a bien été enregistrée');
    }


}
