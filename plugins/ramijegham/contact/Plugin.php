<?php namespace RamiJegham\Contact;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return[
            'RamiJegham\Contact\Components\ContactForm' => 'ContactForm'
        ];
    }

    public function registerSettings()
    {
    }
}
