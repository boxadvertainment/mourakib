@servers(['web' => 'root@robox.agency'])

@setup
    $directory = "/home/mourakib";
    $repository = "git@bitbucket.org:boxadvertainment/mourakib.git";
@endsetup

@macro('create')
    clone
    configure
@endmacro

@macro('deploy')
    pull
    configure
@endmacro

@macro('push')
    ServerPush
@endmacro

@macro('refresh')
    delete
    clone
    configure
@endmacro

@task('clone')
    git clone -b master {{ $repository }} {{ $directory }};

    cd {{ $directory }};
    composer self-update;

    cp .env.production .env;

    composer install --prefer-dist --no-dev --no-interaction;

    echo "Project has been created";
@endtask

@task('ServerPush')
    cd {{ $directory }};
    git add .
    git commit -m 'Server Push'
    git push origin/master';

    echo "Push to git is finished successfully!";
@endtask

@task('pull')
    cd {{ $directory }};

    git pull origin;
    cp .env.production .env;
    composer install --prefer-dist --no-dev --no-interaction;
    php artisan october:up

    echo "Deployment finished successfully!";
@endtask

@task('configure')
    cd {{ $directory }};

    chown -R www-data:www-data {{ $directory }};
    echo "Permissions have been set";
@endtask

@task('delete', ['confirm' => true])
    rm -rf {{ $directory }};
    echo "Project directory has been deleted";
@endtask

@task('reset', ['confirm' => true])
    cd {{ $directory }};
    git reset --hard HEAD;
@endtask

@task('migrate')
    cd {{ $directory }};
    php artisan migrate --force;
@endtask

@task('rollback')
    cd {{ $directory }};
    php artisan migrate:rollback --force;
@endtask

@task('seed')
    cd {{ $directory }};
    php artisan db:seed --force;
@endtask
