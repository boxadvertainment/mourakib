[viewBag]
title = "General Types of Cybercrime"
url = "/bibliotheque/general-types-cybercrime"
layout = "content"
is_hidden = 0
navigation_hidden = 0
localeUrl[en] = "/bibliotheque"
localeUrl[ar] = "/bibliotheque"
==
<h1>Offences against the confidentiality, integrity and availability of computer data and systems</h1>

<h6>&nbsp;</h6>

<p style="text-align: justify;">As discussed in <a href="https://www.unodc.org/e4j/en/cybercrime/module-1/index.html">Module 1</a> on Introduction to Cybercrime, "new" cybercrimes (i.e., <em>cyber-dependent</em> crimes) are primarily those that target systems, networks, and data, and seek to compromise their <em>confidentiality</em> (i.e., systems, networks, and data are protected and only authorized users can access them), <em>integrity</em> (i.e., data is accurate and trustworthy and has not been modified) and <em>availability</em> (i.e., data, services, and systems are accessible on demand). These cybercrimes include hacking; malware creation, possession, and distribution; denial of service (DoS) attacks; distributed denial of service (DDoS) attacks; and website defacement (i.e., a form of online vandalism targeting the content of websites).</p>

<p style="text-align: justify;">
	<br>
</p>

<h4 style="text-align: justify;"><strong>Did you know?</strong></h4>

<p style="text-align: justify;">In the Philippines, the Cybercrime Prevention Act of 2012, Republic Act No. 10175 (RA10175) has a specific provision that classifies crimes defined in the Revised Penal Code (a 1930 law) and special laws which, if committed with the use of ICT, are regarded as cybercrimes and are punishable with penalties one degree higher than the defined penalties under the Revised Penal Code.</p>

<p style="text-align: justify;">Hacking is a term used to describe unauthorized access to systems, networks, and data (hereafter target). Hacking may be perpetrated solely to gain access to a target or to gain and/or maintain such access beyond authorization. Examples of national and regional laws criminalizing intentional unauthorized access (see Cybercrime <a href="https://www.unodc.org/e4j/en/cybercrime/module-3/index.html">Module 3</a> on Legal Frameworks and Human Rights, for information on the levels of criminal culpability as they relate to cybercrime) to a website or information by bypassing security measures are the United Arab Emirates, Article 1 of Federal Law No. 2 of 2006 on the Prevention of Information Technology Crimes, and Article 2 of the Council of Europe's <a href="https://www.coe.int/en/web/conventions/full-list/-/conventions/rms/0900001680081561" target="_blank">Convention on Cybercrime</a> (a.k.a. Budapest Convention; hereafter Cybercrime Convention).</p>

<p style="text-align: justify;">Hackers may also seek unauthorized access to systems to cause damage or other harm to the target. In 2014, Lauri Love, a British hacker, defaced websites, gained unauthorized access to United States Government systems, and stole sensitive information from these systems (Parkin, 2017). This cybercrime compromised the confidentiality of data (by gaining unauthorized access to the website and system and stealing information) and the integrity of data (by defacing websites).&nbsp;</p>

<p style="text-align: justify;">
	<br>
</p>

<h4 style="text-align: justify;"><strong>Cybercriminals Target Plastic Surgery Clients</strong></h4>

<p style="text-align: justify;">Cybercriminals gained unauthorized access to the system of a Lithuanian plastic surgeon and obtained sensitive information about patients from different parts of the world, procedures they undertook, naked photos of the patients, and medical data, among other forms of information (Hern, 2017). The cybercriminals then threatened each patient with the release of this information if the ransom was not paid. The amount of the ransom varied based on the quantity and quality of information about the patient that was stolen.</p>

<p style="text-align: justify;">In addition to unauthorized access to systems, hackers may seek to intercept data as it traverses networks. Article 3 of the Cybercrime Convention proscribes "intentional…interception without right, made by technical means, of non-public transmissions of computer data to, from or within a computer system, including electromagnetic emissions from a computer system carrying such computer data." The illicit interception of data is also prohibited under Article 7 of the Arab League's <a href="http://itlaw.wikia.com/wiki/Arab_Convention_on_Combating_Information_Technology_Offences" target="_blank">Arab Convention on Combating Information Technology Offences</a> of 2010, and Article 29(2)(a) of the <a href="https://au.int/en/treaties/african-union-convention-cyber-security-and-personal-data-protection" target="_blank">African Union Convention on Cyber Security and Personal Data Protection</a> of 2014. An example of illegal interception is a "man-in-the-middle attack," which enables an offender to eavesdrop on communications between the sender and receiver and/or impersonate the sender and/or receiver and communicate on their behalf. This cybercrime compromises the confidentiality of data (through eavesdropping) and integrity of data (by impersonating sender and/or receiver).</p>

<p style="text-align: justify;">
	<br>
</p>

<h4 style="text-align: justify;"><strong>How does a "man-in-the-middle attack" work?</strong></h4>

<p style="text-align: justify;">Offenders hijack connections between clients and servers by creating two connections (offender and client, and offender and server). The purpose of this attack is to surreptitiously intercept, receive, and/or send information between client and server (Maras, 2014, p. 308).</p>

<p style="text-align: justify;">
	<br>
</p>

<p style="text-align: center;"><img src="http://mourakib.org/storage/app/media/uploaded-files/Cybercrime_Module_2_image_1.png" style="width: 714px;" class="fr-fic fr-dib" data-result="success"></p>

<h5 style="text-align: center;"><em>Image source</em>: Veracode. <a href="https://www.veracode.com/security/man-middle-attack" target="_blank">Man-in-the-middle (MITM) attack.</a>&nbsp;</h5>

<h5 style="text-align: center;"><em>Man-in-the-Middle Tutorial: Learn About Man-in-the-Middle Attacks, Vulnerabilities and How to Prevent MITM Attacks.</em></h5>

<h5 style="text-align: right;"><em>&nbsp;</em></h5>

<p style="text-align: justify;">In addition to hacking, cybercriminals can interfere with the functioning of computer systems and/or access to systems, services, and data. Interference can include suppressing, modifying, adding, transmitting, editing, deleting or otherwise damaging data, systems, and services. The Council of Europe Cybercrime Convention prohibits <em>data interference</em>, which is defined as the "intentional … damage[s], deletion, deterioration, alteration or suppression of computer data without right," under Article 4. Data interference is also proscribed under Article 29(2)(a) of the African Union Convention on Cyber Security and Personal Data Protection of 2014, and Article 8 of the Arab League's Arab Convention on Combating Information Technology Offences of 2010.</p>

<p style="text-align: justify;">The Council of Europe Cybercrime Convention also prohibits <em>system interference</em>, which is defined as the "intentional … serious hindering without right of the functioning of a computer system by inputting, transmitting, damaging, deleting, deteriorating, altering or suppressing computer data" (Article 5). This cybercrime is also proscribed under Article 29(1)(d) of the African Union Convention on Cyber Security and Personal Data Protection of 2014. An example of system interference is a <em>denial of service attack</em> (or DoS attack). A DoS attack interferes with systems by overwhelming servers and/or intermediaries (e.g., routers) with requests to prevent legitimate traffic from accessing a site and/or using a system (Maras, 2016, p. 270).</p>

<p style="text-align: justify;">A <em>distributed denial of service attack</em> (or DDoS attack) refers to the use of multiple computers and other digital technologies to conduct coordinated attacks with the intention of overwhelming servers and/or intermediaries to prevent legitimate users' access (Maras, 2016, p. 270-271). An example of how one type of DDoS attack works is as follows (CloudFlare, 2018): Imagine many computers trying to connect to a single computer (the server) all at the same time. The single computer has a limited amount of processing power and network bandwidth. If too many computers try to connect at the same time, the server cannot respond to each connection quickly enough. The result is that the server may not be able to respond to <em>real</em> users because it is too busy with <em>fake</em> requests.</p>

<p style="text-align: justify;">DDoS attacks can be conducted by an individual, group, or state. States can target critical infrastructures, which are deemed essential to the functioning of society. For example, Country A experienced a series of DDoS attacks perpetrated by Country B on its financial sector. As a result of these cyberattacks, citizens of Country A were unable to access online banking, and ATMs within this country were intermittently working.</p>

<p style="text-align: justify;">DDoS attacks are made possible by utilizing digital devices that have been infected with malicious software (or <em>malware</em>) to enable the remote control of these devices and use them to launch cyberattacks. The <em>botnet</em> (i.e., the network of infected digital devices - known as zombies) can be used to commit other cybercrimes, such as <em>cryptojacking</em>. <em>Crytopjacking</em> is a tactic whereby the processing power of infected computers is used to mine <em>cryptocurrency</em> (i.e., encrypted digital currency) for the financial benefit of the person (or persons) controlling the infected digital devices (i.e., the <em>botherder</em>) and/or those who hired the botherders (see Cybercrime <a href="https://www.unodc.org/e4j/en/cybercrime/module-13/key-issues/intro.html">Module 13</a> on Cyber Organized Crime for further information on cryptocurrency).</p>

<p style="text-align: justify;">Cybercriminals may also produce, possess and/or distribute computer misuse tools, including technological devices, malicious software (or malware), and passwords, access codes, and other data that enables individuals to gain illegal access, intercept, or otherwise interfere with the target. Article 9 ("offence of misuse of information technology means") of the Arab Convention on Combating Information Technology Offences criminalizes</p>

<ul>
	<li>(1) the production, sale, purchase, import, distribution or provision of: (a) any tools or programmes designed or adapted for the purpose of committing the offences indicated in Articles 6 [(offence of illicit access), Article 7 (offence of illicit interception), and Article 8 (offence against the integrity of data)]…(b) the information system password, access code or similar information that allows access to the information system with the aim of using it for any of the offences indicated in Articles 6 to 8 … [and] (2) the acquisition of any tools or programmes mentioned in the two paragraphs above with the aim of using them to commit any of the offences indicated in Articles 6 to 8.</li>
</ul>

<p style="text-align: justify;">Likewise, the Council of Europe Cybercrime Convention proscribes</p>

<ul>
	<li style="text-align: justify;">the production, sale, procurement for use, import, distribution or otherwise making available of…a device, including a computer program, designed or adapted primarily for the purpose of committing any of the offences established in accordance with Articles 2 through 5…[and/or] a computer password, access code, or similar data by which the whole or any part of a computer system is capable of being accessed, with intent that it be used for the purpose of committing any of the offences established in Articles 2 through 5… [as well as "the possession of …[these] items ….with [the] intent that …[they] be used for the purpose of committing any of the offences established in Articles 2 through 5 (Article 6).</li>
</ul>

<p style="text-align: justify;">This illicit conduct is described as the <em>misuse of devices</em> in the Council of Europe Cybercrime Convention (Article 6). Under Article 6(3), states "may reserve the right not to" proscribe the conduct listed under Article 6 with the exception of "the sale, distribution or otherwise making available of ["a computer password, access code, or similar data by which the whole or any part of a computer system is capable of being accessed, with intent that it be used for the purpose of committing any of the offences established in Articles 2 through 5"]." In addition, pursuant to Article 6(2) "the production, sale, procurement for use, import, distribution or otherwise making available or possession" of the items listed in Article 6 that are "not for the purpose of committing an offence established in accordance with Articles 2 through 5 of this Convention, such as for the authorized testing or protection of a computer system" shall not be criminalized. This Article, therefore, recognizes the dual use of these tools - they could, for example, be used in a lawful manner, and could also be used in an illicit manner.</p>

<p style="text-align: justify;">National laws vary in their criminalization of the misuse of devices. Some laws cover computer misuse tools possession, creation, distribution, and use, while other countries that have cybercrime laws criminalize some of these actions (UNODC, 2013). The misuse of computer access codes is also not consistently proscribed in national laws (UNODC, 2013).</p>

<p style="text-align: justify;"><em>Malware</em> (or malicious software) is used to infect target systems in order to monitor them, collect data, take control of the system, modify system operation and/or data, and damage the system and/or data. Article 3(b) of the Commonwealth of Independent States' <a href="https://dig.watch/instruments/agreement-cooperation-combating-offences-related-computer-information-commonwealth" target="_blank">Agreement on Cooperation in Combating Offences related to Computer Information</a> of 2001 prohibits the "creation, use or distribution of malicious software." There are several forms of malware that can be used to infect systems (Maras, 2014; Maras, 2016):</p>

<ul>
	<li style="text-align: justify;"><em>Worm</em>. &nbsp;Standalone malicious software that spreads without the need for user activity.&nbsp;</li>
	<li style="text-align: justify;"><em>Virus</em>. &nbsp;Malware that requires user activity to spread (e.g., an executable file with virus spreads when opened by the user).</li>
	<li style="text-align: justify;"><em>Trojan horse</em>. &nbsp;Malware designed to look like legitimate software in order to trick the user into downloading the programme, which infects the users' system to spy, steal and/or cause harm.</li>
	<li style="text-align: justify;"><em>Spyware</em>. &nbsp;Malware designed to surreptitiously monitor infected systems, and collect and relay information back to the creator and/or user of the spyware.</li>
	<li style="text-align: justify;"><em>Ransomware</em>. Malware designed to take users' system, files, and/or data hostage and relinquish control back to the user only after ransom is paid.</li>
	<li style="text-align: justify;"><em>Cryptoransomware</em>&nbsp; (a form of ransomware) is malware that infects a user's digital device, encrypts the user's documents, and threatens to delete files and data if the victim does not pay the ransom.</li>
	<li style="text-align: justify;"><em>Doxware</em>&nbsp; is a form cryptoransomware that perpetrators use against victims that releases the user's data (i.e., makes it public) if ransom is not paid to decrypt the files and data.</li>
</ul>