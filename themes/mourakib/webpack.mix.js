/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your application. See https://github.com/JeffreyWay/laravel-mix.
 |
 */

const mix = require('laravel-mix');
require('mix-env-file');

const assetsPath = 'assets/';


/*
 |--------------------------------------------------------------------------
 | Configuration
 |--------------------------------------------------------------------------
 */
mix
    .options({
        processCssUrls: false,
        autoprefixer: {
            enabled: true,
            options: {
                overrideBrowserslist: ['last 2 versions', '> 1%'],
                cascade: true,
                grid: true,
            }
        },
    })
    .disableNotifications()
    .setPublicPath(assetsPath)
    .webpackConfig({
        resolve: {
            mainFields: ['browser', 'main'],
        },
    })
    .webpackConfig({
        watchOptions: {
            aggregateTimeout: 500,
            poll: 500,
            ignored: ['/node_modules/','plugins','modules','vendor']
        },
        devtool: "cheap-source-map"

    })
    .autoload({
        jquery: ['$', 'window.jQuery', "jQuery", "window.$", "jquery", "window.jquery"]
    });

/*
 |--------------------------------------------------------------------------
 | Browsersync
 |--------------------------------------------------------------------------
 */
mix.browserSync({
    proxy: 'http://mourakib.test',
    files: [assetsPath+'js/**/*.js', assetsPath+'css/**/*.css','themes/mourakib/**/*.htm'],
    reload: true
});

/*
 |--------------------------------------------------------------------------
 | SASS
 |--------------------------------------------------------------------------
 */
mix.sass('src/sass/main.scss', assetsPath+'css');
mix.sass('src/sass/main-rtl.scss', assetsPath+'css');


/*
 |--------------------------------------------------------------------------
 | JS
 |--------------------------------------------------------------------------
 */
mix.js('src/js/app.js', assetsPath+'js');
