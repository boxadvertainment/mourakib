var convo = {
    // "ice" (as in "breaking the ice") is a required conversation object
    // that maps the first thing the bot will say to the user
    ice: {
        // "says" defines an array of sequential bubbles
        // that the bot will produce
        says: [
            "Bonjour!",
            "Je suis <b>Tekness</b> votre assistant virtuel, je suis là pour vous aider",
            "Que souhaitez-vous faire ?"
        ],
        // "reply" is an array of possible options the user can pick from
        // as a reply
        reply: [{
            question: "Signaler un abus en ligne", // label for the reply option
            answer: "openAbuseForm" // key for the next conversation object
        },
        {
            question: "Accéder aux dernières actualités", // label for the reply option
            answer: "openPageActu"
        },
        {
            question: "Consulter la bibliothèque en ligne", // label for the reply option
            answer: "openPageBiblio"
        },
        {
            question: "Nous contacter pour un complément d’information", // label for the reply option
            answer: "openPageContact"
        },
        {
            question: "Inscription à la newsletter", // label for the reply option
            answer: "newsLetter" // key for a "side note" we can reference from multiple points in the chat
        }]
    }, // end required "ice" conversation object

    // side note
    closedReportForm: {
        says: [
            "Vous semblez avoir changé d’avis !",
            "Sachez que de nombreuses questions récurrentes nous sont posées sur les moyens de prévention contre les risques pouvant être rencontrées sur internet.",
            "Si vous le souhaitez bien, je peux vous proposer quelques réponses préétablis.",
        ],
        reply: [
            {
                question: "Oui, ça m’intéresse",
                answer: "security" // key for another side note; in this instance, we’re using it to contextualize the Q&A we’re heading into
            },
            {
                question: "Non, merci.",
                answer: "end" // no further sidetrack required, returning to the main conversation tree
            }
        ]
    },

    closedChat: {
        says: [
            "C’était un plaisir de vous aider ! Si vous avez des questions, n’hésitez pas à me les adresser via la rubrique contact."
        ],
        reply: [
            {
                question: "Non, merci.",
                answer: "closeChat" // key for another side note; in this instance, we’re using it to contextualize the Q&A we’re heading into
            },
            {
                question: "Oui, aller directement à la rubrique contact.",
                answer: "openPageContact" // no further sidetrack required, returning to the main conversation tree
            }
        ]
    },
    FilledReportForm :{
        says:[
            "Merci pour votre signalement",
            "Il a bien été pris en compte",
            "Vous serez recontacté prochainement"
        ]
    },

    // another side note
    "security": {
        says: [
            "Cliquez sur la question à laquelle vous souhaitez avoir une réponse.",
        ],
        reply: [
            {
                question: "1 - Quels sont les formes de fraudes en ligne les plus courantes ?",
                answer: "openPageFraude"
            },
            {
                question: "2 - Comment sécuriser les terminaux mobiles et desktop ?",
                answer: "openPageSecuri"
            },
            {
                question: "3 - Comment prévenir et lutter contre l’exploitation sexuelle des enfants ?",
                answer: "openPageSex"
            }
        ]
    },
    end: {
        says: [
            "Merci pour votre intérêt",
        ]
    }
} // end conversation object
