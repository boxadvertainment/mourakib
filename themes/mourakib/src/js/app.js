/*
 * Application
 */
//Imports
import './cookies';
import Swiper from 'swiper';
import 'bootstrap';
import 'bootstrap-select';



$(function () {
    // ToDo : Create component form each component to load separately.



    let homeSlider = new Swiper('.home-banner', {
        loop: true,
        // effect: 'slide',
        parallax: true,
        speed: 600,
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });


    let partners = new Swiper('.partners-slider', {
        loop: true,
        // effect: 'slide',
        parallax: true,
        speed: 600,
        slidesPerView:1,
        autoplay: {
            delay: 5000,
        },
        breakpoints:{
            880 : {
                slidesPerView: 2
            }
        },
        pagination: {
            el: '.swiper-pagination',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });


    let lastChatAction;


    /*-------------------------------------------------------
     |  Open Report Form
     -------------------------------00----------------------- */

    window.openAbuseForm = function () {
        lastChatAction = 'openReportForm';
        $('#reportFormWrapper,.report-form').addClass('show');
        $('.report-form .form-control')[0].focus();
        $('body').removeClass('reportFormOpen')
    };

    $('[report-trigger]').on('click', function (e) {
        $('#reportFormWrapper,.report-form').addClass('show');
        $('.report-form .form-control')[0].focus();
        $('body').removeClass('reportFormOpen')
    });

    $('[close-report-form]').on('click', function (e) {
        if (lastChatAction === 'openReportForm') {
            lastChatAction ='';
            chatWindow.talk(convo, "closedReportForm")
        }
        $('#reportFormWrapper,.report-form').toggleClass('show');
        $('body').removeClass('reportFormOpen')
    });


    /*-------------------------------------------------------
     |  Custom Select | source : https://developer.snapappointments.com/bootstrap-select/
     -------------------------------00----------------------- */
    $('select').selectpicker();




    $(document).scroll(function () {
        let $body = $('body'),
            scroll = $(this).scrollTop();

        if (scroll >= 120 && ! $body.hasClass('nav-fixed') ) {
            $body.addClass('nav-fixed');
            $('.navbar').addClass('nav-fixed');
        }
        if ( scroll <= 45 && $body.hasClass('nav-fixed') ) {
            $body.removeClass('nav-fixed');
            $('.navbar').removeClass('nav-fixed');
        }
    });


    /*
    responseJSON: {X_OCTOBER_ERROR_FIELDS: {…}, X_OCTOBER_ERROR_MESSAGE: "Le champ email est obligatoire."}

    status: 406 | 200
    statusCode: ƒ (e)
    statusText: "Not Acceptable" | "ok"
    * */
    window.formSubmitted = function (data) {
        //console.log(data,this);
        $('#reportFormWrapper,.report-form').removeClass('show');
        chatWindow.talk(convo, "FilledReportForm")
    };



    /*-------------------------------------------------------
     |  Navbar submenu
     -------------------------------00----------------------- */
    $('.navbar .dropdown-menu a.dropdown-toggle').on('hover', function (e) {
        var $el = $(this);
        var $parent = $(this).offsetParent(".dropdown-menu");
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parent("li").toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-menu .show').removeClass("show");
        });

        if (!$parent.parent().hasClass('navbar-nav')) {
            $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
        }

        return false;
    });


    /*-------------------------------------------------------
     |  Chat
     -------------------------------00----------------------- */

});
